// ye olde compiler passes

use crate::assembly::ast as aast;
use std::collections::HashMap;

pub struct RegisterTracker {
    mapping: HashMap<aast::Identifier, i64>,
    location: i64,
}

impl RegisterTracker {
    // return the offset for a given identifier, if it exists
    // if it doesn't exist, take the next_position as the offset
    // for this register, store it in our mapping, calculate the next position,
    // then return the offset for this register
    pub fn get_or_create(&mut self, iden: aast::Identifier) -> i64 {
        match self.mapping.get(&iden) {
            Some(offset) => *offset,
            None => {
                self.location -= 4;
                self.mapping.insert(iden, self.location);
                self.location
            }
        }
    }

    pub fn new() -> Self {
        RegisterTracker {
            mapping: HashMap::new(),
            location: 0,
        }
    }
}

pub fn replace_instructions(instructions: Vec<aast::Instruction>) -> (Vec<aast::Instruction>, i64) {
    let mut new_instructions = Vec::new();
    let mut tracker = RegisterTracker::new();
    for inst in instructions {
        new_instructions.push(replace_instruction(inst, &mut tracker));
    }
    (new_instructions, tracker.location)
}

pub fn replace_instruction(
    inst: aast::Instruction,
    addresses: &mut RegisterTracker,
) -> aast::Instruction {
    let new_inst = match inst {
        aast::Instruction::AllocateStack(i) => aast::Instruction::AllocateStack(i),
        aast::Instruction::Ret => aast::Instruction::Ret,
        aast::Instruction::Mov {
            src: src_op,
            dest: dest_op,
        } => {
            let src = replace_operand(src_op, addresses);
            let dest = replace_operand(dest_op, addresses);
            aast::Instruction::Mov { src, dest }
        }
        aast::Instruction::Unary {
            op: operator,
            operand: src_operand,
        } => {
            let operand = replace_operand(src_operand, addresses);
            aast::Instruction::Unary {
                op: operator,
                operand,
            }
        }
        aast::Instruction::Binary { op, src, dest } => {
            let src_operand = replace_operand(src, addresses);
            let dest_operand = replace_operand(dest, addresses);
            aast::Instruction::Binary {
                op,
                src: src_operand,
                dest: dest_operand,
            }
        }
        aast::Instruction::Cdq => aast::Instruction::Cdq,
        aast::Instruction::Idiv(op) => aast::Instruction::Idiv(replace_operand(op, addresses)),
        aast::Instruction::Cmp(op1, op2) => aast::Instruction::Cmp(
            replace_operand(op1, addresses),
            replace_operand(op2, addresses),
        ),
        aast::Instruction::Jmp(i) => aast::Instruction::Jmp(i),
        aast::Instruction::JmpCC { cond_code, label } => {
            aast::Instruction::JmpCC { cond_code, label }
        }
        aast::Instruction::SetCC { cond_code, operand } => aast::Instruction::SetCC {
            cond_code,
            operand: replace_operand(operand, addresses),
        },
        aast::Instruction::Label(i) => aast::Instruction::Label(i),
    };
    new_inst
}

fn replace_operand(op: aast::Operand, addresses: &mut RegisterTracker) -> aast::Operand {
    match op {
        aast::Operand::Imm(i) => aast::Operand::Imm(i),
        aast::Operand::Register(r) => aast::Operand::Register(r),
        aast::Operand::Stack(i) => aast::Operand::Stack(i),
        aast::Operand::Pseudo(i) => aast::Operand::Stack(addresses.get_or_create(i)),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_replace_instruction() {
        let inst = aast::Instruction::Mov {
            src: aast::Operand::Imm(2),
            dest: aast::Operand::Pseudo(aast::Identifier("hi".into())),
        };
        let expected_inst = aast::Instruction::Mov {
            src: aast::Operand::Imm(2),
            dest: aast::Operand::Stack(-4),
        };
        let mut addresses = RegisterTracker::new();
        let replaced = replace_instruction(inst, &mut addresses);
        assert_eq!(replaced, expected_inst);
        assert_eq!(
            *addresses
                .mapping
                .get(&aast::Identifier("hi".into()))
                .unwrap(),
            -4
        );

        let mut addresses = RegisterTracker::new();
        let replaced = replace_instruction(aast::Instruction::Ret, &mut addresses);
        assert_eq!(replaced, aast::Instruction::Ret);
        assert_eq!(addresses.mapping.len(), 0);
    }

    #[test]
    fn test_replace_instructions() {
        let instructions = vec![
            aast::Instruction::Mov {
                src: aast::Operand::Imm(2),
                dest: aast::Operand::Pseudo(aast::Identifier("a".into())),
            },
            aast::Instruction::Unary {
                op: aast::UnaryOperator::Neg,
                operand: aast::Operand::Pseudo(aast::Identifier("a".into())),
            },
        ];
        let expected_instructions = vec![
            aast::Instruction::Mov {
                src: aast::Operand::Imm(2),
                dest: aast::Operand::Stack(-4),
            },
            aast::Instruction::Unary {
                op: aast::UnaryOperator::Neg,
                operand: aast::Operand::Stack(-4),
            },
        ];
        let (new_insts, offset) = replace_instructions(instructions);

        assert_eq!(new_insts, expected_instructions);
        assert_eq!(offset, -4);
    }
}
