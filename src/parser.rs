use std::{collections::HashMap, sync::OnceLock};

use crate::lexer::Token;

fn binop_precedence() -> &'static HashMap<&'static Token, u32> {
    static HASHMAP: OnceLock<HashMap<&'static Token, u32>> = OnceLock::new();
    HASHMAP.get_or_init(|| {
        let mut m = HashMap::new();
        m.insert(&Token::Asterisk, 50);
        m.insert(&Token::ForwardSlash, 50);
        m.insert(&Token::Percent, 50);
        m.insert(&Token::Plus, 45);
        m.insert(&Token::Hyphen, 45);
        m.insert(&Token::LessThan, 35);
        m.insert(&Token::LessOrEqual, 35);
        m.insert(&Token::GreaterThan, 35);
        m.insert(&Token::GreaterOrEqual, 35);
        m.insert(&Token::DoubleEqual, 30);
        m.insert(&Token::NotEqual, 30);
        m.insert(&Token::LogicalAnd, 10);
        m.insert(&Token::LogicalOr, 5);
        m.insert(&Token::Equal, 1);
        m
    })
}

pub mod ast {
    #[derive(Debug, PartialEq)]
    pub struct Program {
        pub function_definition: FunctionDefinition,
    }

    #[derive(Debug, PartialEq)]
    pub enum Expr {
        Constant(i64),
        Val {
            variable_name: Identifier,
        },
        Unary {
            op: UnaryOp,
            inner: Box<Expr>,
        },
        Binary {
            op: BinaryOp,
            left: Box<Expr>,
            right: Box<Expr>,
        },
        Assignment {
            lvalue: Box<Expr>,
            exp: Box<Expr>,
        },
    }

    #[derive(Debug, PartialEq)]
    pub enum UnaryOp {
        Negate,
        Complement,
        Not,
    }

    #[derive(Debug, PartialEq)]
    pub enum BinaryOp {
        Add,
        Subtract,
        Multiply,
        Divide,
        Remainder,
        And,
        Or,
        Equal,
        NotEqual,
        LessThan,
        LessOrEqual,
        GreaterThan,
        GreaterOrEqual,
    }

    #[derive(Debug, PartialEq)]
    pub enum Statement {
        Return(Expr),
        Expression(Expr),
        Null,
    }

    #[derive(Debug, PartialEq)]
    pub struct Declaration {
        pub name: Identifier,
        pub init: Option<Expr>,
    }

    #[derive(Debug, PartialEq)]
    pub enum BlockItem {
        Statement(Statement),
        Declaration(Declaration),
    }

    #[derive(Debug, PartialEq)]
    pub struct Identifier(pub String);

    #[derive(Debug, PartialEq)]
    pub struct FunctionDefinition {
        pub name: Identifier,
        pub body: Vec<BlockItem>,
    }
}

pub fn parse(tokens: Vec<Token>) -> Result<ast::Program, ParserError> {
    let mut parser = Parser::new(tokens);
    Ok(program(&mut parser)?)
}

// TODO: add error information like location
#[derive(Debug)]
pub enum ParserError {
    EndOfInput,
    UnexpectedToken(String),
}

impl std::fmt::Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::EndOfInput => write!(f, "EndOfInput"),
            Self::UnexpectedToken(v) => write!(f, "UnexpectedToken: {}", v),
        }
    }
}

type ParseResult<T> = Result<T, ParserError>;

struct Parser {
    tokens: Vec<Token>,
    current: usize,
}

impl Parser {
    fn new(tokens: Vec<Token>) -> Parser {
        Parser { tokens, current: 0 }
    }

    fn peek(&self) -> ParseResult<Token> {
        if self.at_end() {
            Err(ParserError::EndOfInput)
        } else {
            Ok(self.tokens[self.current].clone())
        }
    }

    fn take(&mut self) -> ParseResult<Token> {
        if self.at_end() {
            Err(ParserError::EndOfInput)
        } else {
            let token = self.tokens[self.current].clone();
            self.current += 1;
            Ok(token)
        }
    }

    fn expect(&mut self, expected: Token) -> ParseResult<()> {
        match self.take()? {
            token if token == expected => Ok(()),
            _ => Err(ParserError::UnexpectedToken("expect".into())),
        }
    }

    fn at_end(&self) -> bool {
        self.current == self.tokens.len()
    }
}

fn program(parser: &mut Parser) -> ParseResult<ast::Program> {
    let func = function(parser)?;
    if !parser.at_end() {
        Err(ParserError::UnexpectedToken("not at end of program".into()))
    } else {
        Ok(ast::Program {
            function_definition: func,
        })
    }
}

fn function(parser: &mut Parser) -> ParseResult<ast::FunctionDefinition> {
    parser.expect(Token::KWInt)?;
    let name = identifier(parser)?;
    parser.expect(Token::OpenParen)?;
    parser.expect(Token::KWVoid)?;
    parser.expect(Token::CloseParen)?;
    parser.expect(Token::OpenBrace)?;
    let mut body = Vec::new();
    while parser.peek()? != Token::CloseBrace {
        body.push(block_item(parser)?);
    }
    parser.expect(Token::CloseBrace)?;
    Ok(ast::FunctionDefinition { name, body })
}

fn block_item(parser: &mut Parser) -> ParseResult<ast::BlockItem> {
    match parser.peek()? {
        Token::KWInt => Ok(ast::BlockItem::Declaration(declaration(parser)?)),
        _ => Ok(ast::BlockItem::Statement(statement(parser)?)),
    }
}

fn declaration(parser: &mut Parser) -> ParseResult<ast::Declaration> {
    parser.expect(Token::KWInt)?;
    let name = identifier(parser)?;
    let init = match parser.peek()? {
        Token::Equal => {
            parser.take()?; // consume the Equal token
            Some(expression(parser, 0)?)
        }
        _ => None,
    };
    parser.expect(Token::Semicolon)?;
    Ok(ast::Declaration { name, init })
}

fn identifier(parser: &mut Parser) -> ParseResult<ast::Identifier> {
    match parser.take()? {
        Token::Identifier(iden) => Ok(ast::Identifier(iden)),
        _ => Err(ParserError::UnexpectedToken("identifier".into())),
    }
}

fn statement(parser: &mut Parser) -> ParseResult<ast::Statement> {
    match parser.peek()? {
        Token::KWReturn => {
            parser.expect(Token::KWReturn)?;
            let return_val = expression(parser, 0)?;
            parser.expect(Token::Semicolon)?;
            Ok(ast::Statement::Return(return_val))
        }
        Token::Semicolon => {
            parser.take()?;
            Ok(ast::Statement::Null)
        }
        _ => {
            let expr = expression(parser, 0)?;
            parser.expect(Token::Semicolon)?;
            Ok(ast::Statement::Expression(expr))
        }
    }
}

// the expression and factor functions both yield exp AST nodes; <factor>
// and <exp> are distinct symbols in the grammar, but not different types in
// the AST

// <exp> ::= <factor> | <exp> <binop> <exp>
// when we parse an expression, e1 <op> e2, all the operators in e2
// should have a higher precedence than <op>
fn expression(parser: &mut Parser, min_prec: u32) -> ParseResult<ast::Expr> {
    let mut left = factor(parser)?;
    let mut next_token = parser.peek()?;
    while is_binop(&next_token) && precedence(&next_token) >= min_prec {
        left = if next_token == Token::Equal {
            // advance the parser one token
            parser.expect(Token::Equal)?;
            let right = expression(parser, precedence(&parser.peek()?))?;
            ast::Expr::Assignment {
                lvalue: Box::new(left),
                exp: Box::new(right),
            }
        } else {
            let operator = binop(parser)?;
            let right = expression(parser, precedence(&next_token) + 1)?;
            ast::Expr::Binary {
                op: operator,
                left: Box::new(left),
                right: Box::new(right),
            }
        };
        next_token = parser.peek()?;
    }
    Ok(left)
}

fn is_binop(token: &Token) -> bool {
    binop_precedence().contains_key(token)
}

fn precedence(token: &Token) -> u32 {
    *binop_precedence().get(token).or(Some(&0)).unwrap()
}

fn binop(parser: &mut Parser) -> ParseResult<ast::BinaryOp> {
    match parser.take()? {
        Token::Asterisk => Ok(ast::BinaryOp::Multiply),
        Token::ForwardSlash => Ok(ast::BinaryOp::Divide),
        Token::Percent => Ok(ast::BinaryOp::Remainder),
        Token::Plus => Ok(ast::BinaryOp::Add),
        Token::Hyphen => Ok(ast::BinaryOp::Subtract),
        Token::LogicalAnd => Ok(ast::BinaryOp::And),
        Token::LogicalOr => Ok(ast::BinaryOp::Or),
        Token::DoubleEqual => Ok(ast::BinaryOp::Equal),
        Token::NotEqual => Ok(ast::BinaryOp::NotEqual),
        Token::LessThan => Ok(ast::BinaryOp::LessThan),
        Token::LessOrEqual => Ok(ast::BinaryOp::LessOrEqual),
        Token::GreaterThan => Ok(ast::BinaryOp::GreaterThan),
        Token::GreaterOrEqual => Ok(ast::BinaryOp::GreaterOrEqual),
        _ => Err(ParserError::UnexpectedToken("binop".into())),
    }
}

// <factor> ::= <int> | <unop> <factor> | "(" <exp> ")"
fn factor(parser: &mut Parser) -> ParseResult<ast::Expr> {
    match parser.take()? {
        Token::Constant(val) => Ok(ast::Expr::Constant(val)),
        Token::Tilde => {
            let ex = factor(parser)?;
            Ok(ast::Expr::Unary {
                op: ast::UnaryOp::Complement,
                inner: Box::new(ex),
            })
        }
        Token::Hyphen => {
            let ex = factor(parser)?;
            Ok(ast::Expr::Unary {
                op: ast::UnaryOp::Negate,
                inner: Box::new(ex),
            })
        }
        Token::Bang => {
            let ex = factor(parser)?;
            Ok(ast::Expr::Unary {
                op: ast::UnaryOp::Not,
                inner: Box::new(ex),
            })
        }
        Token::OpenParen => {
            let ex = expression(parser, 0)?;
            parser.expect(Token::CloseParen)?;
            Ok(ex)
        }
        Token::Identifier(value) => Ok(ast::Expr::Val {
            variable_name: ast::Identifier(value),
        }),
        _ => Err(ParserError::UnexpectedToken("factor".into())),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_expression() {
        let tokens = vec![
            Token::Constant(1),
            Token::Asterisk,
            Token::Constant(2),
            Token::Plus,
            Token::Constant(3),
            Token::Semicolon,
        ];
        let mut parser = Parser::new(tokens);
        let got = expression(&mut parser, 0).unwrap();
        let want = ast::Expr::Binary {
            op: ast::BinaryOp::Add,
            left: Box::new(ast::Expr::Binary {
                op: ast::BinaryOp::Multiply,
                left: Box::new(ast::Expr::Constant(1)),
                right: Box::new(ast::Expr::Constant(2)),
            }),
            right: Box::new(ast::Expr::Constant(3)),
        };
        assert_eq!(got, want);

        let tokens = vec![
            Token::Identifier("a".into()),
            Token::Equal,
            Token::Identifier("b".into()),
            Token::Equal,
            Token::Identifier("c".into()),
            Token::Semicolon,
        ];
        let mut parser = Parser::new(tokens);
        let got = expression(&mut parser, 0).unwrap();
        let want = ast::Expr::Assignment {
            lvalue: Box::new(ast::Expr::Val {
                variable_name: ast::Identifier("a".into()),
            }),
            exp: Box::new(ast::Expr::Assignment {
                lvalue: Box::new(ast::Expr::Val {
                    variable_name: ast::Identifier("b".into()),
                }),
                exp: Box::new(ast::Expr::Val {
                    variable_name: ast::Identifier("c".into()),
                }),
            }),
        };
        assert_eq!(got, want);
    }

    #[test]
    fn test_parse_program() {
        let tokens = vec![
            Token::KWInt,
            Token::Identifier("main".into()),
            Token::OpenParen,
            Token::KWVoid,
            Token::CloseParen,
            Token::OpenBrace,
            Token::KWReturn,
            Token::Tilde,
            Token::Constant(0),
            Token::Semicolon,
            Token::CloseBrace,
        ];
        let mut parser = Parser::new(tokens);
        let got = program(&mut parser).unwrap();
        let want = ast::Program {
            function_definition: ast::FunctionDefinition {
                name: ast::Identifier("main".into()),
                body: vec![ast::BlockItem::Statement(ast::Statement::Return(
                    ast::Expr::Unary {
                        op: ast::UnaryOp::Complement,
                        inner: Box::new(ast::Expr::Constant(0)),
                    },
                ))],
            },
        };
        assert_eq!(got, want);
    }
}
