use std::collections::HashMap;
use std::error::Error;
use std::sync::OnceLock;

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, alphanumeric1, digit1, multispace0},
    combinator::{not, peek, recognize, value},
    multi::many0_count,
    sequence::{delimited, pair, terminated},
    IResult, Parser,
};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Token {
    // keywords
    KWInt,
    KWVoid,
    KWReturn,

    // literals, identifiers, things of that nature
    Identifier(String),
    Constant(i64),

    // symbols
    OpenParen,
    CloseParen,
    OpenBrace,
    CloseBrace,
    Semicolon,
    Tilde,
    Hyphen,
    DoubleHyphen,
    Plus,
    Asterisk,
    ForwardSlash,
    Percent,
    Bang,
    LogicalAnd,
    LogicalOr,
    Equal,
    DoubleEqual,
    NotEqual,
    LessThan,
    GreaterThan,
    LessOrEqual,
    GreaterOrEqual,
}

fn keywords() -> &'static HashMap<&'static str, Token> {
    static HASHMAP: OnceLock<HashMap<&'static str, Token>> = OnceLock::new();
    HASHMAP.get_or_init(|| {
        let mut m = HashMap::new();
        m.insert("int", Token::KWInt);
        m.insert("void", Token::KWVoid);
        m.insert("return", Token::KWReturn);
        m
    })
}

pub fn lex(input: &str) -> Result<Vec<Token>, Box<dyn Error + '_>> {
    let mut result = Vec::new();
    let mut remaining = input;
    let mut parser = delimited(multispace0, get_next_token, multispace0);
    while remaining.len() > 0 {
        let (rem, matched) = parser(remaining)?;
        remaining = rem;
        result.push(matched);
    }
    Ok(result)
}

fn get_next_token(input: &str) -> IResult<&str, Token> {
    alt((identifier, constant, symbol))(input)
}

fn constant(input: &str) -> IResult<&str, Token> {
    let (remaining, constant) = terminated(digit1, peek(not(alpha1)))(input)?;
    Ok((remaining, Token::Constant(constant.parse::<i64>().unwrap())))
}

fn identifier(input: &str) -> IResult<&str, Token> {
    let (remaining, text) = recognize(pair(
        alt((alpha1, tag("_"))),
        many0_count(alt((alphanumeric1, tag("_")))),
    ))
    .parse(input)?;
    if let Some(kw) = keywords().get(text) {
        Ok((remaining, kw.clone()))
    } else {
        Ok((remaining, Token::Identifier(text.into())))
    }
}

fn symbol(input: &str) -> IResult<&str, Token> {
    alt((
        value(Token::Semicolon, tag(";")),
        value(Token::Tilde, tag("~")),
        value(Token::DoubleHyphen, tag("--")),
        value(Token::Hyphen, tag("-")),
        value(Token::Plus, tag("+")),
        value(Token::Asterisk, tag("*")),
        value(Token::ForwardSlash, tag("/")),
        value(Token::Percent, tag("%")),
        value(Token::OpenParen, tag("(")),
        value(Token::CloseParen, tag(")")),
        value(Token::OpenBrace, tag("{")),
        value(Token::CloseBrace, tag("}")),
        alt((
            value(Token::DoubleEqual, tag("==")),
            value(Token::Equal, tag("=")),
        )),
        value(Token::NotEqual, tag("!=")),
        value(Token::LogicalAnd, tag("&&")),
        value(Token::LogicalOr, tag("||")),
        value(Token::LessOrEqual, tag("<=")),
        value(Token::LessThan, tag("<")),
        value(Token::GreaterOrEqual, tag(">=")),
        value(Token::GreaterThan, tag(">")),
        value(Token::Bang, tag("!")),
    ))(input)
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn test_ident() {
        let (remaining_input, output) = identifier("int main(void)").unwrap();
        assert_eq!(remaining_input, " main(void)");
        assert_eq!(output, Token::KWInt);

        let result = identifier("(parens)");
        assert!(result.is_err());

        let result = identifier("id0");
        assert!(result.is_ok());
        assert_eq!(result.unwrap().1, Token::Identifier("id0".into()));

        let result = identifier("0");
        assert!(result.is_err());

        let result = identifier("1foo");
        assert!(result.is_err());
    }

    #[test]
    fn test_constant() {
        let (remaining, ten) = constant("10").unwrap();
        assert_eq!(remaining, "");
        assert_eq!(ten, Token::Constant(10));

        let result = constant("1foo");
        assert!(result.is_err());

        let (remaining, hundo) = constant("100;").unwrap();
        assert_eq!(remaining, ";");
        assert_eq!(hundo, Token::Constant(100));
    }

    #[test]
    fn test_symbol() {
        let (remaining, lparen) = symbol("({})").unwrap();
        assert_eq!(remaining, "{})");
        assert_eq!(lparen, Token::OpenParen);

        let (remaining, lbrace) = symbol(remaining).unwrap();
        assert_eq!(remaining, "})");
        assert_eq!(lbrace, Token::OpenBrace);

        let (remaining, rbrace) = symbol(remaining).unwrap();
        assert_eq!(remaining, ")");
        assert_eq!(rbrace, Token::CloseBrace);

        let (remaining, rparen) = symbol(remaining).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(rparen, Token::CloseParen);

        let (remaining, bit_comp) = symbol("~").unwrap();
        assert_eq!(remaining, "");
        assert_eq!(bit_comp, Token::Tilde);

        let (remaining, neg) = symbol("-").unwrap();
        assert_eq!(remaining, "");
        assert_eq!(neg, Token::Hyphen);

        let (remaining, dec) = symbol("--").unwrap();
        assert_eq!(remaining, "");
        assert_eq!(dec, Token::DoubleHyphen);

        let (remaining, not_eq) = symbol("!=>>==").unwrap();
        assert_eq!(remaining, ">>==");
        assert_eq!(not_eq, Token::NotEqual);
        let (remaining, gt) = symbol(remaining).unwrap();
        assert_eq!(remaining, ">==");
        assert_eq!(gt, Token::GreaterThan);
        let (remaining, gte) = symbol(remaining).unwrap();
        assert_eq!(remaining, "=");
        assert_eq!(gte, Token::GreaterOrEqual);
        let (remaining, eq) = symbol(remaining).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(eq, Token::Equal);

        let (remaining, eq_eq) = symbol("==").unwrap();
        assert_eq!(remaining, "");
        assert_eq!(eq_eq, Token::DoubleEqual);
    }

    #[test]
    fn test_get_next_token() {
        let (remaining, oparen) = get_next_token("(void)").unwrap();
        let (remaining, iden) = get_next_token(remaining).unwrap();
        let (remaining, cparen) = get_next_token(remaining).unwrap();
        assert_eq!(remaining, "");
        assert_eq!(oparen, Token::OpenParen);
        assert_eq!(iden, Token::KWVoid);
        assert_eq!(cparen, Token::CloseParen);
    }

    #[test]
    fn text_lex() {
        let tokens = lex("int main(void)").unwrap();
        assert_eq!(
            tokens,
            vec![
                Token::KWInt,
                Token::Identifier("main".into()),
                Token::OpenParen,
                Token::KWVoid,
                Token::CloseParen
            ]
        );

        let source = "int main(void) {
    return ~0;
}
";
        let tokens = lex(source).unwrap();
        assert_eq!(
            tokens,
            vec![
                Token::KWInt,
                Token::Identifier("main".into()),
                Token::OpenParen,
                Token::KWVoid,
                Token::CloseParen,
                Token::OpenBrace,
                Token::KWReturn,
                Token::Tilde,
                Token::Constant(0),
                Token::Semicolon,
                Token::CloseBrace
            ]
        );
    }
}
