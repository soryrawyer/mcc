use crate::assembly::ast as aast;
use crate::replace_pseudo::replace_instructions;

// two things:
// 1. insert an aast::Instruction::AllocateStack instruction into function
//    definitions. the value passed to AllocateStack is the absolute value
//    of the value returned by our replace_pseudoregisters function
// 2. rewrite invalid Mov instructions. if both source and destination
//    fields for a Mov instruction are `aast::Operand::Stack`s, then we need
//    to break up that instruction into two: the first moves a value into a
//    temporary register (we'll use aast::Reg::R10 for that) and then a second
//    instruction to move the value from the temporary register (again, R10)
//    to the original destination
pub fn fixup_instructions(prog: aast::Program) -> aast::Program {
    let (insts, offset) = replace_instructions(prog.function_definition.instructions);
    let mut new_instructions = vec![aast::Instruction::AllocateStack(offset.abs())];
    for inst in insts {
        new_instructions.extend(fix_instruction(inst));
    }
    aast::Program {
        function_definition: aast::FunctionDefinition {
            name: prog.function_definition.name,
            instructions: new_instructions,
        },
    }
}

fn fix_instruction(inst: aast::Instruction) -> Vec<aast::Instruction> {
    match inst {
        aast::Instruction::Mov {
            src: aast::Operand::Stack(s),
            dest: aast::Operand::Stack(d),
        } => {
            vec![
                aast::Instruction::Mov {
                    src: aast::Operand::Stack(s),
                    dest: aast::Operand::Register(aast::Reg::R10),
                },
                aast::Instruction::Mov {
                    src: aast::Operand::Register(aast::Reg::R10),
                    dest: aast::Operand::Stack(d),
                },
            ]
        }
        aast::Instruction::Mov { src, dest } => vec![aast::Instruction::Mov { src, dest }],
        aast::Instruction::Unary { op, operand } => vec![aast::Instruction::Unary { op, operand }],
        aast::Instruction::AllocateStack(i) => vec![aast::Instruction::AllocateStack(i)],
        aast::Instruction::Ret => vec![aast::Instruction::Ret],
        aast::Instruction::Cdq => vec![aast::Instruction::Cdq],
        // idiv cannot take a constant operand
        aast::Instruction::Idiv(aast::Operand::Imm(i)) => {
            vec![
                aast::Instruction::Mov {
                    src: aast::Operand::Imm(i),
                    dest: aast::Operand::Register(aast::Reg::R10),
                },
                aast::Instruction::Idiv(aast::Operand::Register(aast::Reg::R10)),
            ]
        }
        aast::Instruction::Idiv(op) => vec![aast::Instruction::Idiv(op)],
        // multiplication instructions can't have any kind of memory address
        // as its destination, so we'll move the destination register's contents
        // into the temporary register R11, multiply/store the result there,
        // then move that value back into our destination register
        aast::Instruction::Binary {
            op: aast::BinaryOperator::Mult,
            src,
            dest,
        } => {
            vec![
                aast::Instruction::Mov {
                    src: dest.clone(),
                    dest: aast::Operand::Register(aast::Reg::R11),
                },
                aast::Instruction::Binary {
                    op: aast::BinaryOperator::Mult,
                    src: src,
                    dest: aast::Operand::Register(aast::Reg::R11),
                },
                aast::Instruction::Mov {
                    src: aast::Operand::Register(aast::Reg::R11),
                    dest: dest,
                },
            ]
        }
        // like move instructions, binary instructions can't handle two things on the stack at once
        aast::Instruction::Binary {
            op,
            src: aast::Operand::Stack(s),
            dest: aast::Operand::Stack(d),
        } => {
            vec![
                aast::Instruction::Mov {
                    src: aast::Operand::Stack(s),
                    dest: aast::Operand::Register(aast::Reg::R10),
                },
                aast::Instruction::Binary {
                    op,
                    src: aast::Operand::Register(aast::Reg::R10),
                    dest: aast::Operand::Stack(d),
                },
            ]
        }
        aast::Instruction::Binary { op, src, dest } => {
            vec![aast::Instruction::Binary { op, src, dest }]
        }
        aast::Instruction::Cmp(op1, aast::Operand::Imm(i)) => {
            // TODO: the example here uses %eax as the first operand
            // so let's make sure to check this out if anything looks awry
            vec![
                aast::Instruction::Mov {
                    src: aast::Operand::Imm(i),
                    dest: aast::Operand::Register(aast::Reg::R11),
                },
                aast::Instruction::Cmp(op1, aast::Operand::Register(aast::Reg::R11)),
            ]
        }
        aast::Instruction::Cmp(op1, op2) => {
            vec![
                aast::Instruction::Mov {
                    src: op1,
                    dest: aast::Operand::Register(aast::Reg::R10),
                },
                aast::Instruction::Cmp(aast::Operand::Register(aast::Reg::R10), op2),
            ]
        }
        aast::Instruction::Jmp(l) => vec![aast::Instruction::Jmp(l)],
        aast::Instruction::JmpCC { cond_code, label } => {
            vec![aast::Instruction::JmpCC { cond_code, label }]
        }
        aast::Instruction::SetCC { cond_code, operand } => {
            vec![aast::Instruction::SetCC { cond_code, operand }]
        }
        aast::Instruction::Label(l) => vec![aast::Instruction::Label(l)],
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_fix_instruction() {
        let instruction = aast::Instruction::Mov {
            src: aast::Operand::Stack(-4),
            dest: aast::Operand::Stack(-8),
        };
        let expected = vec![
            aast::Instruction::Mov {
                src: aast::Operand::Stack(-4),
                dest: aast::Operand::Register(aast::Reg::R10),
            },
            aast::Instruction::Mov {
                src: aast::Operand::Register(aast::Reg::R10),
                dest: aast::Operand::Stack(-8),
            },
        ];
        let actual = fix_instruction(instruction);
        assert_eq!(actual, expected);

        let instruction = aast::Instruction::Mov {
            src: aast::Operand::Imm(-4),
            dest: aast::Operand::Stack(-8),
        };
        let expected = vec![instruction.clone()];
        let actual = fix_instruction(instruction);
        assert_eq!(actual, expected);
    }
}
