use crate::parser::ast as past;

pub mod ast {
    #[derive(Debug, PartialEq)]
    pub struct Program {
        pub function_definition: FunctionDefinition,
    }

    #[derive(Debug, PartialEq)]
    pub struct FunctionDefinition {
        pub name: Identifier,
        pub instructions: Vec<Instruction>,
    }

    #[derive(Debug, PartialEq)]
    pub enum Instruction {
        Return(Val),
        Unary {
            operator: UnaryOperator,
            src: Val,
            dest: Val,
        },
        Binary {
            operator: BinaryOperator,
            src1: Val,
            src2: Val,
            dest: Val,
        },
        Copy {
            src: Val,
            dest: Val,
        },
        Jump(Identifier),
        JumpIfZero {
            condition: Val,
            target: Identifier,
        },
        JumpIfNotZero {
            condition: Val,
            target: Identifier,
        },
        Label(Identifier),
    }

    #[derive(Debug, PartialEq)]
    pub enum UnaryOperator {
        Complement,
        Negate,
        Not,
    }

    #[derive(Debug, PartialEq)]
    pub enum BinaryOperator {
        Add,
        Subtract,
        Multiply,
        Divide,
        Remainder,
        Equal,
        NotEqual,
        LessThan,
        LessOrEqual,
        GreaterThan,
        GreaterOrEqual,
    }

    #[derive(Clone, Debug, PartialEq)]
    pub struct Identifier(pub String);

    #[derive(Clone, Debug, PartialEq)]
    pub enum Val {
        Constant(i64),
        Var(Identifier),
    }
}

pub fn emit_tacky(node: past::Program) -> ast::Program {
    let mut emitter = TackyEmitter::new();
    emitter.program(node)
}

enum IdentifierType {
    Label,
    Variable,
}

struct TackyEmitter {
    tmp_var_count: u32,
    label_count: u32,
}

impl TackyEmitter {
    fn new() -> Self {
        TackyEmitter {
            tmp_var_count: 0,
            label_count: 0,
        }
    }

    fn make_temporary(&mut self, itype: IdentifierType) -> ast::Identifier {
        let name = match itype {
            IdentifierType::Label => {
                self.label_count += 1;
                format!("tmp.{}", self.label_count)
            }
            IdentifierType::Variable => {
                self.tmp_var_count += 1;
                format!("tmp.{}", self.tmp_var_count)
            }
        };
        ast::Identifier(name)
    }

    fn program(&mut self, node: past::Program) -> ast::Program {
        ast::Program {
            function_definition: self.function_definition(node.function_definition),
        }
    }

    fn function_definition(&mut self, func: past::FunctionDefinition) -> ast::FunctionDefinition {
        ast::FunctionDefinition {
            name: ast::Identifier(func.name.0),
            instructions: todo!("handle change in function body"),
            // instructions: self.instructions(func.body),
        }
    }

    fn instructions(&mut self, stmt: past::Statement) -> Vec<ast::Instruction> {
        match stmt {
            past::Statement::Return(e) => {
                let (mut instructions, dst) = self.expr(e);
                instructions.push(ast::Instruction::Return(dst));
                instructions
            }
            _ => todo!("handle new types of statements"),
        }
    }

    fn expr(&mut self, exp: past::Expr) -> (Vec<ast::Instruction>, ast::Val) {
        match exp {
            past::Expr::Constant(c) => (vec![], ast::Val::Constant(c)),
            past::Expr::Unary { op, inner } => {
                let (mut instructions, src) = self.expr(*inner);
                let dest = ast::Val::Var(self.make_temporary(IdentifierType::Variable));
                let operator = unary_to_unary(op);
                instructions.push(ast::Instruction::Unary {
                    operator,
                    src,
                    dest: dest.clone(),
                });
                (instructions, dest)
            }
            past::Expr::Binary {
                op: past::BinaryOp::And,
                left,
                right,
            } => {
                // TODO: reuse literally _any_ of this, please
                // 1) gen instructions for left
                let mut instructions = Vec::new();
                let (i1, ldest) = self.expr(*left);
                instructions.extend(i1);
                // 2) generate Label for where the result is gonna geaux
                let false_label = self.make_temporary(IdentifierType::Label);
                instructions.push(ast::Instruction::JumpIfZero {
                    condition: ldest,
                    target: false_label.clone(),
                });
                // 3) gen instructions for right
                let (i2, rdest) = self.expr(*right);
                instructions.extend(i2);
                // 4) jump to earlier Label if the result is zero
                instructions.push(ast::Instruction::JumpIfZero {
                    condition: rdest,
                    target: false_label.clone(),
                });
                // 5) otherwise, say the result is 1 and then jump to the end
                let dest = ast::Val::Var(self.make_temporary(IdentifierType::Variable));
                instructions.push(ast::Instruction::Copy {
                    src: ast::Val::Constant(1),
                    dest: dest.clone(),
                });
                let end_label = self.make_temporary(IdentifierType::Label);
                instructions.push(ast::Instruction::Jump(end_label.clone()));
                instructions.push(ast::Instruction::Label(false_label));
                instructions.push(ast::Instruction::Copy {
                    src: ast::Val::Constant(0),
                    dest: dest.clone(),
                });
                instructions.push(ast::Instruction::Label(end_label));
                (instructions, dest)
            }
            past::Expr::Binary {
                op: past::BinaryOp::Or,
                left,
                right,
            } => {
                // TODO:
                // 1) gen instructions for left
                let mut instructions = Vec::new();
                let (i1, ldest) = self.expr(*left);
                instructions.extend(i1);
                // 2) generate Label for where the result is gonna geaux
                let true_label = self.make_temporary(IdentifierType::Label);
                instructions.push(ast::Instruction::JumpIfNotZero {
                    condition: ldest,
                    target: true_label.clone(),
                });
                // 3) gen instructions for right
                let (i2, rdest) = self.expr(*right);
                instructions.extend(i2);
                // 4) jump to earlier Label if the result is zero
                instructions.push(ast::Instruction::JumpIfNotZero {
                    condition: rdest,
                    target: true_label.clone(),
                });
                // 5) otherwise, say the result is 1 and then jump to the end
                let dest = ast::Val::Var(self.make_temporary(IdentifierType::Variable));
                instructions.push(ast::Instruction::Copy {
                    src: ast::Val::Constant(0),
                    dest: dest.clone(),
                });
                let end_label = self.make_temporary(IdentifierType::Label);
                instructions.push(ast::Instruction::Jump(end_label.clone()));
                instructions.push(ast::Instruction::Label(true_label));
                instructions.push(ast::Instruction::Copy {
                    src: ast::Val::Constant(1),
                    dest: dest.clone(),
                });
                instructions.push(ast::Instruction::Label(end_label));
                (instructions, dest)
            }
            past::Expr::Binary { op, left, right } => {
                let mut instructions = Vec::new();
                let (i1, src1) = self.expr(*left);
                instructions.extend(i1);
                let (i2, src2) = self.expr(*right);
                instructions.extend(i2);
                let dest = ast::Val::Var(self.make_temporary(IdentifierType::Variable));
                let operator = binary_to_binary(op);
                instructions.push(ast::Instruction::Binary {
                    operator,
                    src1,
                    src2,
                    dest: dest.clone(),
                });
                (instructions, dest)
            }
            _ => todo!("handle new types of expressions"),
        }
    }
}

fn unary_to_unary(op: past::UnaryOp) -> ast::UnaryOperator {
    match op {
        past::UnaryOp::Negate => ast::UnaryOperator::Negate,
        past::UnaryOp::Complement => ast::UnaryOperator::Complement,
        past::UnaryOp::Not => ast::UnaryOperator::Not,
    }
}

fn binary_to_binary(op: past::BinaryOp) -> ast::BinaryOperator {
    match op {
        past::BinaryOp::Add => ast::BinaryOperator::Add,
        past::BinaryOp::Subtract => ast::BinaryOperator::Subtract,
        past::BinaryOp::Divide => ast::BinaryOperator::Divide,
        past::BinaryOp::Multiply => ast::BinaryOperator::Multiply,
        past::BinaryOp::Remainder => ast::BinaryOperator::Remainder,
        past::BinaryOp::Equal => ast::BinaryOperator::Equal,
        past::BinaryOp::NotEqual => ast::BinaryOperator::NotEqual,
        past::BinaryOp::LessThan => ast::BinaryOperator::LessThan,
        past::BinaryOp::LessOrEqual => ast::BinaryOperator::LessOrEqual,
        past::BinaryOp::GreaterThan => ast::BinaryOperator::GreaterThan,
        past::BinaryOp::GreaterOrEqual => ast::BinaryOperator::GreaterOrEqual,
        past::BinaryOp::And => panic!("logical AND is not a tacky binary operator"),
        past::BinaryOp::Or => panic!("logical OR is not a tacky binary operator"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_stmt() {
        let mut emitter = TackyEmitter::new();
        let parser_ast = past::Statement::Return(past::Expr::Constant(0));
        let expected = vec![ast::Instruction::Return(ast::Val::Constant(0))];
        let actual = emitter.instructions(parser_ast);
        assert_eq!(actual, expected);

        let parser_ast = past::Statement::Return(past::Expr::Unary {
            op: past::UnaryOp::Complement,
            inner: Box::new(past::Expr::Constant(2)),
        });
        let expected = vec![
            ast::Instruction::Unary {
                operator: ast::UnaryOperator::Complement,
                src: ast::Val::Constant(2),
                dest: ast::Val::Var(ast::Identifier("tmp.1".into())),
            },
            ast::Instruction::Return(ast::Val::Var(ast::Identifier("tmp.1".into()))),
        ];
        let actual = emitter.instructions(parser_ast);
        assert_eq!(actual, expected);

        let parser_ast = past::Statement::Return(past::Expr::Unary {
            op: past::UnaryOp::Negate,
            inner: Box::new(past::Expr::Unary {
                op: past::UnaryOp::Complement,
                inner: Box::new(past::Expr::Unary {
                    op: past::UnaryOp::Negate,
                    inner: Box::new(past::Expr::Constant(8)),
                }),
            }),
        });
        let expected = vec![
            ast::Instruction::Unary {
                operator: ast::UnaryOperator::Negate,
                src: ast::Val::Constant(8),
                dest: ast::Val::Var(ast::Identifier("tmp.1".into())),
            },
            ast::Instruction::Unary {
                operator: ast::UnaryOperator::Complement,
                src: ast::Val::Var(ast::Identifier("tmp.1".into())),
                dest: ast::Val::Var(ast::Identifier("tmp.2".into())),
            },
            ast::Instruction::Unary {
                operator: ast::UnaryOperator::Negate,
                src: ast::Val::Var(ast::Identifier("tmp.2".into())),
                dest: ast::Val::Var(ast::Identifier("tmp.3".into())),
            },
            ast::Instruction::Return(ast::Val::Var(ast::Identifier("tmp.3".into()))),
        ];
        let mut emitter = TackyEmitter::new();
        let actual = emitter.instructions(parser_ast);
        assert_eq!(actual, expected);
    }
}
