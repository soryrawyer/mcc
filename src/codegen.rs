use crate::assembly::ast as aast;

pub fn program(prog: aast::Program) -> String {
    let func = function_definition(prog.function_definition);
    format!(
        "{}\n\t.section\t.note.GNU-stack,\"\",@progbits\n",
        func.as_str()
    )
}

fn function_definition(func: aast::FunctionDefinition) -> String {
    let mut result = format!("\t.globl {}\n", func.name.0);
    result.push_str(format!("{}:\n", func.name.0).as_str());
    result.push_str("\tpushq %rbp\n");
    result.push_str("\tmovq %rsp, %rbp\n");
    for instruct in func.instructions {
        if let Some(insts) = instruction(instruct) {
            let instructions = insts
                .into_iter()
                // TODO: omit the tab if the instruction is a label
                .map(|i| {
                    if i.starts_with(".L") {
                        i
                    } else {
                        format!("\t{}", i)
                    }
                })
                .collect::<Vec<String>>()
                .join("\n");
            result.push_str(format!("{}\n", instructions).as_str());
        }
    }
    result
}

fn instruction(inst: aast::Instruction) -> Option<Vec<String>> {
    match inst {
        aast::Instruction::Mov { src, dest } => Some(vec![format!(
            "movl {}, {}",
            operand(src, ByteLevel::Four),
            operand(dest, ByteLevel::Four)
        )]),
        // TODO: figure out how to actually use str references here
        aast::Instruction::Ret => Some(vec![
            "movq %rbp, %rsp".into(),
            "popq %rbp".into(),
            "ret".into(),
        ]),
        aast::Instruction::Unary {
            op,
            operand: unary_operand,
        } => Some(vec![format!(
            "{} {}",
            unary_op_to_instruction(op),
            operand(unary_operand, ByteLevel::Four)
        )]),
        aast::Instruction::AllocateStack(0) => None,
        aast::Instruction::AllocateStack(i) => Some(vec![format!("subq ${}, %rsp", i)]),
        aast::Instruction::Binary { op, src, dest } => Some(vec![format!(
            "{} {}, {}",
            binary_op_to_instruction(op),
            operand(src, ByteLevel::Four),
            operand(dest, ByteLevel::Four)
        )]),
        aast::Instruction::Idiv(i) => Some(vec![format!("idivl {}", operand(i, ByteLevel::Four))]),
        aast::Instruction::Cdq => Some(vec!["cdq".into()]),
        aast::Instruction::Cmp(op1, op2) => Some(vec![format!(
            "cmpl {}, {}",
            operand(op1, ByteLevel::Four),
            operand(op2, ByteLevel::Four)
        )]),
        aast::Instruction::Jmp(aast::Identifier(label)) => Some(vec![format!("jmp .L{}", label)]),
        aast::Instruction::JmpCC {
            cond_code: cc,
            label: aast::Identifier(label),
        } => Some(vec![format!("j{} .L{}", cond_code(cc), label)]),
        aast::Instruction::SetCC {
            cond_code: cc,
            operand: op,
        } => Some(vec![format!(
            "set{} {}",
            cond_code(cc),
            operand(op, ByteLevel::One)
        )]),
        aast::Instruction::Label(aast::Identifier(label)) => Some(vec![format!(".L{}:", label)]),
    }
}

fn cond_code(cc: aast::CondCode) -> String {
    String::from(match cc {
        aast::CondCode::Equal => "e",
        aast::CondCode::NotEqual => "ne",
        aast::CondCode::Less => "l",
        aast::CondCode::LessOrEqual => "le",
        aast::CondCode::Greater => "g",
        aast::CondCode::GreaterOrEqual => "ge",
    })
}

fn unary_op_to_instruction(op: aast::UnaryOperator) -> String {
    match op {
        aast::UnaryOperator::Neg => "negl".into(),
        aast::UnaryOperator::Not => "notl".into(),
    }
}

fn binary_op_to_instruction(op: aast::BinaryOperator) -> String {
    match op {
        aast::BinaryOperator::Add => "addl".into(),
        aast::BinaryOperator::Sub => "subl".into(),
        aast::BinaryOperator::Mult => "imull".into(),
    }
}

enum ByteLevel {
    One,
    Four,
}

fn operand(op: aast::Operand, bl: ByteLevel) -> String {
    match (op, bl) {
        (aast::Operand::Imm(i), _) => format!("${}", i),
        (aast::Operand::Register(aast::Reg::AX), ByteLevel::One) => "%al".into(),
        (aast::Operand::Register(aast::Reg::AX), ByteLevel::Four) => "%eax".into(),
        (aast::Operand::Register(aast::Reg::DX), ByteLevel::One) => "%dl".into(),
        (aast::Operand::Register(aast::Reg::DX), ByteLevel::Four) => "%edx".into(),
        (aast::Operand::Register(aast::Reg::R10), ByteLevel::One) => "%r10b".into(),
        (aast::Operand::Register(aast::Reg::R10), ByteLevel::Four) => "%r10d".into(),
        (aast::Operand::Register(aast::Reg::R11), ByteLevel::One) => "%r11b".into(),
        (aast::Operand::Register(aast::Reg::R11), ByteLevel::Four) => "%r11d".into(),
        (aast::Operand::Stack(i), _) => format!("{}(%rbp)", i),
        (aast::Operand::Pseudo(_), _) => {
            panic!("trying to generate code containing a pseudoregister")
        }
    }
}
