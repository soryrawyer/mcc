use crate::tacky::ast as tast;

pub mod ast {
    #[derive(Debug, PartialEq)]
    pub struct Program {
        pub function_definition: FunctionDefinition,
    }

    #[derive(Debug, PartialEq)]
    pub struct FunctionDefinition {
        pub name: Identifier,
        pub instructions: Vec<Instruction>,
    }

    #[derive(Clone, Debug, Eq, Hash, PartialEq)]
    pub struct Identifier(pub String);

    #[derive(Clone, Debug, PartialEq)]
    pub enum Instruction {
        // This represents the number of bytes we need to subtract from the stack pointer
        // in our function prologues (e.g. subq $n, %rsp)
        AllocateStack(i64),
        Mov {
            src: Operand,
            dest: Operand,
        },
        Ret,
        Unary {
            op: UnaryOperator,
            operand: Operand,
        },
        Binary {
            op: BinaryOperator,
            src: Operand,
            dest: Operand,
        },
        Cmp(Operand, Operand),
        Idiv(Operand),
        Cdq, // sign extension from EAX to EDX (fills in bits)
        Jmp(Identifier),
        JmpCC {
            cond_code: CondCode,
            label: Identifier,
        },
        SetCC {
            cond_code: CondCode,
            operand: Operand,
        },
        Label(Identifier),
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum CondCode {
        Equal,
        NotEqual,
        Greater,
        GreaterOrEqual,
        Less,
        LessOrEqual,
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum UnaryOperator {
        Neg,
        Not,
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum BinaryOperator {
        Add,
        Sub,
        Mult,
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum Operand {
        // the operand is an immediate value
        Imm(i64),
        // the operand is in a register (and presumably needs to be loaded)
        Register(Reg),
        // Pseudoregisters represent the temporary variables assigned in our TACKY
        // representation. Since they're not real registers, these need to be gone
        // from the final assembly program
        Pseudo(Identifier),
        // this represents a location on the stack. the integer within this variant
        // refers to the offset from the current function's stack (e.g. -4(%rbp))
        Stack(i64),
    }

    // the hardware registers available to us
    #[derive(Clone, Debug, PartialEq)]
    pub enum Reg {
        AX,
        DX,
        R10,
        R11,
    }
}

pub fn tacky_to_assembly(c_program: tast::Program) -> ast::Program {
    program(c_program)
}

fn program(c_program: tast::Program) -> ast::Program {
    ast::Program {
        function_definition: function(c_program.function_definition),
    }
}

fn function(func: tast::FunctionDefinition) -> ast::FunctionDefinition {
    let mut instructions: Vec<ast::Instruction> = Vec::new();
    for inst in func.instructions.iter() {
        let new_instructions = instruction(inst);
        instructions.extend(new_instructions);
    }
    ast::FunctionDefinition {
        name: ast::Identifier(func.name.0),
        instructions,
    }
}

fn instruction(inst: &tast::Instruction) -> Vec<ast::Instruction> {
    match inst {
        tast::Instruction::Return(val) => vec![
            ast::Instruction::Mov {
                src: operand(val.clone()),
                dest: ast::Operand::Register(ast::Reg::AX),
            },
            ast::Instruction::Ret,
        ],
        tast::Instruction::Unary {
            operator: tast::UnaryOperator::Not,
            src,
            dest,
        } => {
            vec![
                ast::Instruction::Cmp(ast::Operand::Imm(0), operand(src.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::Equal,
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::Instruction::Unary {
            operator,
            src,
            dest,
        } => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src.clone()),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::Unary {
                    op: unary(operator),
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::Instruction::Binary {
            operator,
            src1,
            src2,
            dest,
        } => {
            // For addition, subtraction, and multiplication, we convert a single
            // TACKY instruction into two assembly instructions.
            convert_binary(operator, src1, src2, dest)
        }
        tast::Instruction::Jump(tast::Identifier(label)) => {
            vec![ast::Instruction::Jmp(ast::Identifier(label.clone()))]
        }
        tast::Instruction::JumpIfZero {
            // condition: tast::Val::Var(tast::Identifier(id)),
            condition,
            target: tast::Identifier(t),
        } => {
            vec![
                ast::Instruction::Cmp(
                    ast::Operand::Imm(0),
                    operand(condition.clone()),
                    // ast::Operand::Pseudo(ast::Identifier(id.to_string())),
                ),
                ast::Instruction::JmpCC {
                    cond_code: ast::CondCode::Equal,
                    label: ast::Identifier(t.to_string()),
                },
            ]
        }
        // tast::Instruction::JumpIfZero { .. } => panic!("invalid JumpIfZero args"),
        tast::Instruction::JumpIfNotZero {
            // condition: tast::Val::Var(tast::Identifier(id)),
            condition,
            target: tast::Identifier(t),
        } => {
            vec![
                ast::Instruction::Cmp(
                    ast::Operand::Imm(0),
                    operand(condition.clone()),
                    // ast::Operand::Pseudo(ast::Identifier(id.to_string())),
                ),
                ast::Instruction::JmpCC {
                    cond_code: ast::CondCode::NotEqual,
                    label: ast::Identifier(t.to_string()),
                },
            ]
        }
        // tast::Instruction::JumpIfNotZero { .. } => panic!("invalid JumpIfNotZero args"),
        tast::Instruction::Label(tast::Identifier(label)) => {
            vec![ast::Instruction::Label(ast::Identifier(label.clone()))]
        }
        tast::Instruction::Copy { src, dest } => {
            vec![ast::Instruction::Mov {
                src: operand(src.clone()),
                dest: operand(dest.clone()),
            }]
        }
    }
}

fn convert_binary(
    bi: &tast::BinaryOperator,
    src1: &tast::Val,
    src2: &tast::Val,
    dest: &tast::Val,
) -> Vec<ast::Instruction> {
    match bi {
        tast::BinaryOperator::Add => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src1.clone()),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::Binary {
                    op: ast::BinaryOperator::Add,
                    src: operand(src2.clone()),
                    dest: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::Subtract => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src1.clone()),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::Binary {
                    op: ast::BinaryOperator::Sub,
                    src: operand(src2.clone()),
                    dest: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::Multiply => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src1.clone()),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::Binary {
                    op: ast::BinaryOperator::Mult,
                    src: operand(src2.clone()),
                    dest: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::Divide => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src1.clone()),
                    dest: ast::Operand::Register(ast::Reg::AX),
                },
                ast::Instruction::Cdq,
                ast::Instruction::Idiv(operand(src2.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Register(ast::Reg::AX),
                    dest: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::Remainder => {
            vec![
                ast::Instruction::Mov {
                    src: operand(src1.clone()),
                    dest: ast::Operand::Register(ast::Reg::AX),
                },
                ast::Instruction::Cdq,
                ast::Instruction::Idiv(operand(src2.clone())),
                ast::Instruction::Mov {
                    // note: we _do_ want DX here, and not AX like we used above,
                    // because DX will have the remainder while AX will have the quotient
                    src: ast::Operand::Register(ast::Reg::DX),
                    dest: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::Equal => {
            vec![
                ast::Instruction::Cmp(operand(src1.clone()), operand(src2.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::Equal,
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::NotEqual => {
            vec![
                ast::Instruction::Cmp(operand(src1.clone()), operand(src2.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::NotEqual,
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::LessThan => {
            vec![
                ast::Instruction::Cmp(operand(src2.clone()), operand(src1.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::Less,
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::LessOrEqual => vec![
            ast::Instruction::Cmp(operand(src2.clone()), operand(src1.clone())),
            ast::Instruction::Mov {
                src: ast::Operand::Imm(0),
                dest: operand(dest.clone()),
            },
            ast::Instruction::SetCC {
                cond_code: ast::CondCode::LessOrEqual,
                operand: operand(dest.clone()),
            },
        ],
        tast::BinaryOperator::GreaterThan => {
            vec![
                ast::Instruction::Cmp(operand(src2.clone()), operand(src1.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::Greater,
                    operand: operand(dest.clone()),
                },
            ]
        }
        tast::BinaryOperator::GreaterOrEqual => {
            vec![
                ast::Instruction::Cmp(operand(src2.clone()), operand(src1.clone())),
                ast::Instruction::Mov {
                    src: ast::Operand::Imm(0),
                    dest: operand(dest.clone()),
                },
                ast::Instruction::SetCC {
                    cond_code: ast::CondCode::GreaterOrEqual,
                    operand: operand(dest.clone()),
                },
            ]
        }
    }
}

fn unary(un: &tast::UnaryOperator) -> ast::UnaryOperator {
    match un {
        tast::UnaryOperator::Complement => ast::UnaryOperator::Not,
        tast::UnaryOperator::Negate => ast::UnaryOperator::Neg,
        tast::UnaryOperator::Not => todo!("support NOT operator from chapter 4"),
    }
}

fn operand(val: tast::Val) -> ast::Operand {
    match val {
        tast::Val::Constant(i) => ast::Operand::Imm(i),
        tast::Val::Var(id) => ast::Operand::Pseudo(ast::Identifier(id.0)),
    }
}
