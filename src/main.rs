use clap::{Parser, Subcommand};
use std::{
    ffi::OsString,
    fs::{self, File},
    io::prelude::*,
    process::ExitCode,
};

#[derive(Debug, Parser)]
#[command(version, about)]
struct Args {
    /// input source file to run the compiler on
    filename: String,

    /// which stage of the compiler to run
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand, PartialEq)]
enum Commands {
    Codegen,
    Full { path: OsString },
    Lex,
    Parse,
    Tacky,
}

fn main() -> ExitCode {
    let args = Args::parse();
    let source = fs::read_to_string(&args.filename)
        .expect(format!("unable to read from {}", &args.filename).as_ref());
    // TODO: look up how to nicely handle converting errors to an appropriate ExitCode
    // that way, we could chain the lex/parse/etc function calls together
    match args.command {
        Commands::Lex => match mcc::lexer::lex(&source) {
            Ok(_) => ExitCode::SUCCESS,
            Err(_) => ExitCode::FAILURE,
        },
        Commands::Parse => {
            let tokens = mcc::lexer::lex(&source).expect("expected file to lex properly");
            // TODO: implement pretty printing for ASTs
            match mcc::parser::parse(tokens) {
                Ok(_) => ExitCode::SUCCESS,
                Err(_) => ExitCode::FAILURE,
            }
        }
        Commands::Codegen => {
            let tokens = mcc::lexer::lex(&source).expect("expected file to lex properly");
            let c_nodes = mcc::parser::parse(tokens).expect("expected file to parse properly");
            let tacky_ir = mcc::tacky::emit_tacky(c_nodes);
            let assembly_nodes = mcc::assembly::tacky_to_assembly(tacky_ir);
            let passed_nodes = mcc::fixup::fixup_instructions(assembly_nodes);
            let _ = mcc::codegen::program(passed_nodes);
            ExitCode::SUCCESS
        }
        Commands::Full { path } => {
            let tokens = mcc::lexer::lex(&source).expect("expected file to lex properly");
            let c_nodes = mcc::parser::parse(tokens).expect("expected file to parse properly");
            let tacky_ir = mcc::tacky::emit_tacky(c_nodes);
            let assembly_nodes = mcc::assembly::tacky_to_assembly(tacky_ir);
            let passed_nodes = mcc::fixup::fixup_instructions(assembly_nodes);
            let final_result = mcc::codegen::program(passed_nodes);
            let mut file = File::create(path).expect("uhhh I should be able to create a file");
            file.write_all(final_result.as_bytes())
                .expect("UHJHHH I Should be able to write here");
            ExitCode::SUCCESS
        }
        Commands::Tacky => {
            let tokens = mcc::lexer::lex(&source).expect("expected file to lex properly");
            let c_nodes = mcc::parser::parse(tokens).expect("expected file to parse properly");
            let _ = mcc::tacky::emit_tacky(c_nodes);
            ExitCode::SUCCESS
        }
    }
}
