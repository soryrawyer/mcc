#!/usr/bin/env python3
"""
compiler driver for mcc
"""
import argparse
import os
import subprocess
import sys


def _parse_args():
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("source_path")
    parser.add_argument("--lex", action="store_true")
    parser.add_argument("--parse", action="store_true")
    parser.add_argument("--tacky", action="store_true")
    parser.add_argument("--codegen", action="store_true")
    parser.add_argument("-S", action="store_true")
    return parser.parse_args()


def main():
    args = _parse_args()
    (executable, _) = os.path.splitext(args.source_path)
    preprocessed_output = f"{executable}.i"
    preprocess = ["gcc", "-E", "-P", args.source_path, "-o", preprocessed_output]
    subprocess.call(preprocess)
    compiler = ["/home/rory/dev/writing-a-c-compiler/mcc/target/debug/mcc", preprocessed_output]
    if args.lex:
        compiler.append("lex")
    elif args.parse:
        compiler.append("parse")
    elif args.tacky:
        compiler.append("tacky")
    elif args.codegen:
        compiler.append("codegen")
    else:
        compiler.append("full")
        compiler.append(f"{executable}.s")
    result = subprocess.run(compiler)
    if result.returncode != 0:
        sys.exit(result.returncode)
    assemble = ["gcc", f"{executable}.s", "-o", executable]
    subprocess.call(assemble)


if __name__ == "__main__":
    main()
