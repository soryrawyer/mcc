# mcc - my c compiler

## building and running the compiler
`cargo build` will build the binary for the compiler. here is the help text output:

``` sh
Usage: mcc <FILENAME> <COMMAND>

Commands:
  lex      
  parse    
  codegen  
  tacky    
  help     Print this message or the help of the given subcommand(s)

Arguments:
  <FILENAME>  input source file to run the compiler on

Options:
  -h, --help     Print help
  -V, --version  Print version
```


the file [`driver.py`](./driver.py) is what ends up getting used for the [corresponding test suite](https://github.com/nlsandler/writing-a-c-compiler-tests/). here is an example invocation of that test suite:

``` sh
./test_compiler /home/rory/dev/writing-a-c-compiler/mcc/driver.py --chapter 2 --stage lex
```

## project layout
each compiler stage has a dedicated module. if the compiler stage requires a new AST then the AST should be defined in a nested module called `ast` (e.g. `parser::ast`, `tacky::ast`, `assembly::ast`).
